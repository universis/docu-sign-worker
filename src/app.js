import express from 'express';
import cors from 'cors';
import path from 'path';
import {ViewEngine} from '@themost/ejs';
import {proxyRouter} from './proxyRouter';

function getApplication() {
    // init app
    const app = express();
    // https://github.com/expressjs/cors#usage
    app.use(cors({
        credentials: true,
        origin: true
    }));

    // https://expressjs.com/en/guide/using-template-engines.html
    app.engine('ejs', ViewEngine.express());
    app.set('view engine', 'ejs');
    app.set('views', path.resolve(__dirname, 'views'));

    app.get('/', (req, res) => {
        return res.render('index');
    });

    app.use('/Sign/Api/', proxyRouter({
        serviceHost: 'https://sapi.mindigital-shde.gr'
    }));

    // and return
    return app;
}

export {
    getApplication
}