import express from 'express';
import { createProxyMiddleware } from 'http-proxy-middleware';

/**
 * 
 * @param {{serviceHost:string}} config 
 * @returns 
 */
export function proxyRouter(config) {
    const router = express.Router();
    router.use(createProxyMiddleware({
        target: config.serviceHost,
        changeOrigin: true
    }));
    return router;
}
